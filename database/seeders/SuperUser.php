<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class SuperUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "Sair Sanchez",
            'email' => "desarrollador4@albertoalvarez.com", 
            'password' => bcrypt("123456789*")
        ]);
    }
}
