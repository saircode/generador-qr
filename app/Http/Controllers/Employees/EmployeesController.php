<?php

namespace App\Http\Controllers\Employees;

use App\Http\Controllers\Controller;
use App\Imports\EmployeesImport;
use App\Models\Employe\Employe;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class EmployeesController extends Controller
{
    public function index(){
        $employees = Employe::latest()->get();

        $micarpeta = storage_path("app/public/img/qrcode/");
        if (!file_exists($micarpeta)) {
            mkdir($micarpeta, 0777, true);
        }else {

            $gestor = opendir(storage_path("app/public/img/qrcode/"));
            while (($archivo = readdir($gestor)) !== false)  {
                // Solo buscamos archivos sin entrar en subdirectorios
                if (is_file(storage_path("app/public/img/qrcode/")."/".$archivo)) {
                    unlink(storage_path("app/public/img/qrcode/")."/".$archivo);
                }            
            }
        }

        foreach ($employees as $key => $item) {
            QrCode::size(500)
            ->format('png')
            ->color(8, 21, 40)
            //->backgroundColor(226, 111, 12)
            ->generate($item["cedula"], storage_path("app/public/img/qrcode/".$item['cedula'].".png"));
        }
        
        return Inertia("qrgenerator/index", [
            "employees" => $employees,
            "path" => public_path("/storage/img/qrcode/")
        ]);
    }

    public function upload(Request $request) {
        
        if($request->file){
            //->Primero elimino todos los usuarios que existían de una versión anterior
        $employees = Employe::latest()->get();

        foreach ($employees as $key => $value) {
            $employe = Employe::find($value->id);
            $employe->delete();
        }

           try {
                Excel::import(new EmployeesImport, request()->file("file"));
           } catch (\Throwable $th) {
               throw $th;
           }
        }
        
    }
}
