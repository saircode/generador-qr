<?php

namespace App\Http\Controllers;

use App\Models\Employe\Employe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Inertia\Inertia;
use ZipArchive;

class CreateZipController extends Controller
{
    public function index()
    {   

        $Dir = public_path("/storage/zip");
        if (!file_exists($Dir)) {
            mkdir($Dir, 0777, true);
        }

        
        $zipFileName = date("M-y").time().'.zip';
        $zip = new ZipArchive;


        if ($zip->open($Dir . '/' . $zipFileName, ZipArchive::CREATE) === TRUE) {


            // Add File in ZipArchive
            $invoice_files =  public_path("/storage/img/qrcode/*");

           

            $files = glob($invoice_files);

            foreach ($files as $file)
            {
                $zip->addFile($file, basename($file));
            }


            $zip->close();
        }else {
            return response("No se pudo crear el archivo zip", 422);
        }


        // Set Header


        $headers = array(


            'Content-Type' => 'application/octet-stream',


        );


        $filetopath = $Dir . '/' . $zipFileName;


        // Create Download Response
        $employees = Employe::latest()->get();

        if (file_exists($filetopath)) {
            // return response()->download( $filetopath  );
            return Inertia("qrgenerator/index", [
                "employees" => $employees,
                "fileName" => $zipFileName
            ]);
        }

        

        return Inertia("qrgenerator/index", [
            "employees" => $employees
        ]);
    }
}
