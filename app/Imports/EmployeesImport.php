<?php
namespace App\Imports;

use App\Models\Employe\Employe;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpParser\Node\Expr\Empty_;

class EmployeesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        
        

        //dd($row);
        $Employe = Employe::create([
            
            'cedula' => str_replace(".", "",$row[0] ) ,
            'name' => $row[1]
        ]);
        return $Employe;
       
    }
}
